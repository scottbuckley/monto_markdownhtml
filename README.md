# README #

This is a simple Monto server (via montolib.py) that uses the markdown2 package to turn 'markdown' versions into 'html' products.


### Dependencies ###
* [MontoLib](https://bitbucket.org/inkytonik/monto)
* [Markdown2 for Python](https://pypi.python.org/pypi/Markdown)


Here's a screenshot of it working (HTML rendering via PyJSMonto):
![Screen Shot 2014-11-03 at 11.51.05 pm.png](https://bitbucket.org/repo/kGj497/images/1399732049-Screen%20Shot%202014-11-03%20at%2011.51.05%20pm.png)