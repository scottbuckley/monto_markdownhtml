# markdown_to_html
# Converts any versions of language 'markdown' into a HTML product.

import montolib, markdown2

def markdown_to_html (version):
    product = markdown2.markdown(version['contents'])
    return [('markdownHTML', 'html', product, True)]

montolib.server (markdown_to_html, 'markdown')

